from __future__ import print_function

# HIDScope-Server
__author__ = 'Tom Lankhorst <t.j.w.lankhorst@student.utwente.nl>'
__version__ = '2.1.1'
from config import *
from modules.ScopeDataServer import scope_data_server
from modules.AssetsServer import assets_server
from modules.ControllerServer import controller_server

print("HIDScope-Server", __version__)

controller = controller_server()
scope_data_server(controller)
assets_server()

print('Navigate to http://localhost:', assets_server_port, '/ to use the scope', sep='')








