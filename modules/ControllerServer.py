__author__ = 'Tom Lankhorst'

from config import *
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
import modules.hid as hid
import json, threading, sys


def send_headers(self, is_json = True):
    if is_json:
        self.send_header('Content-Type', 'application/json')
    self.send_header('Access-Control-Allow-Origin', '*')
    self.send_header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-Request, Content-Type, Accept')
    self.end_headers()

class ControllerServer(BaseHTTPRequestHandler):

    state = {
        'path': None,
        'device': None
    }

    def do_OPTIONS(self):
        self.send_response(200)
        self.send_header('allow', 'GET')
        send_headers(self,False)

    def do_GET(self):

        print "Controller request:", self.path

        if self.path == '/getDevices':
            self.send_response(200)
            send_headers(self)

            hid_devices = [];
            for device in hid.core.find_all_hid_devices():
                hid_devices.append({
                    "name": repr(device),
                    "path": device.device_path,
                    "manufacturer": device.vendor_name,
                    "product": device.product_name,
                })

            self.wfile.write(json.dumps({
                "devices": hid_devices
            }))

        elif self.path == '/getSelectedDevice':
            self.send_response(200)
            send_headers(self)

            self.wfile.write(json.dumps({
                "path": self.state['path'] if self.state['path'] != None else False
            }))

        else:
            self.send_response(404)
            send_headers(self)


class ThreadedControllerServer(ThreadingMixIn, HTTPServer):
    allow_reuse_address = True

    def shutdown(self):
        self.socket.close()
        ControllerServer.shutdown(self)

# Server init fn
def controller_server():
    print "Starting Controller Data Server at port", controller_server_port
    server = ThreadedControllerServer(('127.0.0.1',controller_server_port), ControllerServer)
    t = threading.Thread(target=server.serve_forever)
    t.start()
    return ControllerServer