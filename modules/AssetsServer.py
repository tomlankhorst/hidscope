__author__ = 'Tom Lankhorst'

from config import *
import SimpleHTTPServer, SocketServer, threading, os

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
httpd = SocketServer.TCPServer(("", assets_server_port), Handler)

def assets_server():
    print "Starting Assets Server at port", assets_server_port
    os.chdir('resources/assets')
    t = threading.Thread(target=httpd.serve_forever)
    t.daemon = True
    t.start()
