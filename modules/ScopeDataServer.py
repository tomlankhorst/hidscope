__author__ = 'Tom Lankhorst'

from config import *
from modules.SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
from time import sleep
import time
import modules.hid as hid
import threading, json, struct
from array import *

controller = None

# Socket server definition
class ScopeDataServer(WebSocket):

    # Datahandler and buffer for max 6 ch
    chBuffer = [[],[],[],[],[],[]]
    readFrames = 0
    send_time = int(round(time.time() * 1000))

    def handleMessage(self):
        request = json.loads(self.data)

        if request['method'] == 'setDevice':

            # Set the current HID device
            if controller.state['path'] == None:
                controller.state['path'] = request['data']['path']
                controller.state['device'] = hid.core.HidDevice(request['data']['path'])
                print 'Now connected to: ', controller.state['path']
            else:
                print 'Can\'t switch device in running mode'

            # Handle incoming data
            try:
                if not controller.state['device'].is_opened():
                    controller.state['device'].open()
                controller.state['device'].set_raw_data_handler(self.scope_data_handler)
            finally:
                sleep(0.3)

    def handleConnected(self):
        print self.address, 'connected'
        print 'Currently connected to: ', controller.state['path']

    def handleClose(self):
        print self.address, 'closed'

    def scope_data_handler(self,b):
        self.readFrames += 1
        v = array('f')
        v.fromstring(struct.pack("64B", *b[1:65]))
        for i in range(0,6):
            self.chBuffer[i].append(v[i])

        now_time = int(round(time.time() * 1000));
        if self.readFrames > 0 and now_time - self.send_time > scope_data_server_timeout_ms:
            self.send_time = now_time;
            self.readFrames = 0
            self.sendMessage(json.dumps(self.chBuffer).decode('utf8'))
            self.chBuffer = [[],[],[],[],[],[]]

# Server init fn
def scope_data_server(ctrl):
    global controller
    controller = ctrl
    print "Starting Scope Data Server at port", scope_data_server_port
    server = SimpleWebSocketServer('', scope_data_server_port, ScopeDataServer)
    t = threading.Thread(target=server.serveforever)
    t.start()
