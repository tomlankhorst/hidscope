HID Scope
=========

Using this application you can visualize signals to and from a MBED-enabled device. Import the [MBED library](http://mbed.org/users/tomlankhorst/code/HIDScope/) into your project and set it up to send some data (see the [example](http://mbed.org/users/tomlankhorst/code/HIDScope/docs/e44574634162/classHIDScope.html)). This scope visualisation is browser based, the server is written in Python.

Quick Start
===========
- Download the precompiled application (https://bitbucket.org/tomlankhorst/hidscope/downloads/hidscope-server-v2.1.zip)
- Extract the ZIP-archive
- Run server.exe

Changelog
=========
2015-09-12  v2.1    Added 'overlay' functionality for viewing multiple signals  
2015-05-19  v2.0    Rewritten to Python for better Windows compatibility
2014-09-19  v1.0    Basic functionality, written in NodeJS using HIDAPI

Running from Source
===================
Alternatively the program can be run from source
- Download Python 2.7
- Clone the repo
- Run `python server.py`

Building stand-alone package
============================
Using [PyInstaller](https://github.com/pyinstaller/pyinstaller/wiki)
`pyinstaller-script.py server.py server.spec`