/* 
 * The MIT License
 *
 * Copyright 2014 Tom Lankhorst.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var options = {
    jsonServer: 'http://localhost:18080/',
    websocket:  'ws://localhost:18081/'
};


/**
 * Initialize the device selector
 */
window.addEvent('domready', function(){
    
    // Initialize the refresh button
    $('refreshDevices').addEvent('click', function(e){
        
        // Prevent scroll due to anchor hash
        if(typeof(e)!=='undefined')
            e.preventDefault();
        
        // Set the selector to -- Loading -- state
        var deviceSelector = $('deviceSelect')
                .empty()
                .set('disabled', 'disabled')
                .grab(Elements.from('<option>-- Loading --</option>')[0]);
                
        // Load the devices (from Node server)
        new Request.JSON({
            url: options.jsonServer+'getDevices',
            onSuccess: function(data){
            
                deviceSelector
                        .empty()
                        .grab(Elements.from('<option value="">-- Select a device --</option>')[0]);

                // Add all devices to selector list
                data.devices.each(function( device , key ){
                    deviceSelector.grab(Elements.from('<option value="' + device.path + '">' + device.manufacturer + ' - ' + device.product + '</option>')[0]);
                });

                // Check if a device is already selected
                new Request.JSON({
                    url: options.jsonServer+'getSelectedDevice',
                    onSuccess: function(data){
                        if(data.path){
                            deviceSelector.set('value', data.path);
                        } else {
                            deviceSelector.erase('disabled');
                        }
                    }
                }).get();

            }
        }).get();
    }).fireEvent('click');  // Trigger the button 
    
    // Initialize the connect button
    $('connectButton').addEvent('click', function(e){
        
        // Prevent scrolling
        e.preventDefault();
        
        var error       = false;
        var devicePath  = $('deviceSelect').get('value');
        var channels    = parseInt($('channelSelect').get('value'));
        
        // Check for device path
        if(!devicePath)
            error = 'Select a valid device';
        
        // Check for errors
        if(!error){
            $('deviceSelector').setStyle('display', 'none');
            $('scope').setStyle('display', 'block');
            
            // No errors, start the scope
            initScope(devicePath, channels);
            
        } else {
            
            // Show errors
            $('scopeAlert').setStyle('display', 'block').set('text', error);
        }
    });
    
});

// Global variables
var websocket = null;
var scope = null;

/**
 * initScope 
 * Initialze the scope
 * - Sets the selected device at the NodeJS server
 * - Initializes scope object
 * @param {string} scopePath - Hardware path of the HID device
 * @param {int} scopeChannels - Number of channels (up to 6)
 */
function initScope(scopePath, scopeChannels)
{
    // Initialze the websocket
    websocket = websocket = new WebSocket(options.websocket);
    
    // Initialize the scope
    scope = new Scope(scopeChannels, 'scope');
    
    // Attach events
    websocket.onopen = function(evt) { 
        websocket.send(JSON.stringify({
                method: "setDevice",
                data: {
                    path: scopePath
                }
        }));
    };
    websocket.onclose = function(evt) { 
        // If the connection closes, warn the user to restart the system
        alert('Connection to the server is lost! Maybe the device was disconnected or the server crashed?');
        $(document.body).empty().set('text', 'Please restart the server');
    }; 
    websocket.onerror = function(evt) {
        // There was an error
        alert('There was an error, the event is dumped to the console. Please restart the server');
        console.log(evt);
        $(document.body).empty().set('text', 'Please restart the server');
    };
    websocket.onmessage = function(evt){ scope.handleData(evt); };
    
    // On resize, resize the scope too
    $(window).addEvent('resize', scope.handleResize.bind(scope));
    
}

/**
 * Scope class
 * 
 * @type Class
 */
var Scope = new Class({
    
    // Some options
    fps:        60,     // Target FPS
    autoScale:  100,    // ms between auto-scaling
    yLines:     8,      // y-axis lines
    
    container:  null,
    channels:   0, 
    autoScaleTimer: null, 
    fpsTimer:   null,
    dataBuffer: [],
    chOptions: [],
    chElements: [],
    chSettings: [],
    chCanvasses: [],
    chContexts: [],
    scopeWidth: 0,
    states: [],
    overlayChannels: false,

    strokeStyles: [
        '#FFFF36',
        '#FF36A8',
        '#8DFF36',
        '#36BCFF',
        '#EBFFAD',
        '#FFADEF'
    ],

    /**
     * Initialize the scope
     * @param {int} channels
     * @param {string} containterId
     */
    initialize: function(channels, containterId)
    {
        
        // Set the container and channel count
        this.container  = $(containterId);
        this.channels   = channels;

        // Initialize default options, turn on all scopes (state=1)
        for(var i=0;i<channels;i++){
            this.initChOptions(i);
            this.states[i] = 1; 
        }
        
        // Draw the scope elements
        this.drawChannelElements();
        
        // Trigger the first resize
        this.handleResize();
        
        // Initialize timers for drawing and scaling
        this.fpsTimer = setInterval(this.redraw.bind(this), 1000/this.fps);
        this.autoScaleTimer = setInterval(this.doAutoScale.bind(this), this.autoScale);
        
        
    },

    // setOverlayMode
    setOverlayMode: function()
    {

        var set = this.enableOverlayBtn.checked;

        if( set === this.overlayChannels )
            return;

        this.overlayChannels = set;

        // sync settings
        for(var i=0;i<this.channels;i++){
            this.updateConfig( i );

            if( set && i !== 0 ){
                this.chElements[i].setStyle('display', 'none');
            } else {
                this.chElements[i].setStyle('display', 'block');
            }

        }

        this.handleResize();


    },

    // Function called to draw channels
    drawChannelElements: function()
    {
        // For every channel
        for(var i=0;i<this.channels;i++){
            
            // Parent container
            var chParent = new Element('div', {
                'class': 'scopeUnit',
                'data-chid': i
            });
            
            // Channel header
            var scopeInfo = new Element('div', {
                'text': 'Channel ' + (i),
                'class': 'info'
            });
            
            // Drawing canvas
            var scopeCanvas = new Element('canvas', {
                'class': 'display'
            });
            
            // Settings container
            var scopeSettings = new Element('div', {
                'class': 'settings'
            });
            
            // Copy the settings from the template (HTML)
            var settings = $('settingsTemplate').clone().erase('id');
            this.chSettings.push(settings);
            
            // Construct the parent
            chParent
                .grab(scopeInfo)
                .grab(scopeCanvas)
                .grab(scopeSettings.grab(settings));
            
            // Hook up some values and events
            settings.getElement('.config_samples').set('value', this.chOptions[i].bufferSize);
            settings.getElement('.config_ymin').set('value', this.chOptions[i].yAxis[0]);
            settings.getElement('.config_ymax').set('value', this.chOptions[i].yAxis[1]);
            settings.getElement('.config_autoscale').set('checked', this.chOptions[i].autoscale);
            settings.getElement('.config_btn_update').addEvent('click', this.updateConfig.pass([i], this));
            settings.getElement('.config_btn_start').addEvent('click', function(i){ this.states[i] = 1; }.pass([i],this));
            settings.getElement('.config_btn_stop').addEvent('click', function(i){ this.states[i] = 0; }.pass([i],this));
            settings.getElement('.config_btn_copy').addEvent('click', this.copyBuffer.pass([i], this));

            // Store canvasses, contexts and channelcontainers
            this.chCanvasses.push(scopeCanvas);
            this.chContexts.push(scopeCanvas.getContext('2d'));
            this.chElements.push(chParent);
            
            // Inject channel in container
            chParent.inject(this.container);
            
        };
        
        var masterControls = $('general_settings');
        
        masterControls.getElement('.config_btn_update').addEvent('click', function(){
            for(var i=0;i<this.channels;i++){
                this.updateConfig.pass([i], this)();
            }
        }.bind(this));
        
        masterControls.getElement('.config_btn_start').addEvent('click', function(){
            for(var i=0;i<this.channels;i++){
                (function(i){ this.states[i] = 1; }).pass([i],this)();
            }
        }.bind(this));
        
        masterControls.getElement('.config_btn_stop').addEvent('click', function(){
            for(var i=0;i<this.channels;i++){
                (function(i){ this.states[i] = 0; }).pass([i],this)();
            }
        }.bind(this));

        this.enableOverlayBtn = $('enable-overlay')
                                    .addEvent('change', this.setOverlayMode.bind(this));
        
    },
    
    /**
     * Get channel options
     * @param {int} ch channel no
     * @returns {object} options 
     */
    getChOptions: function(ch)
    {
        return this.chOptions[ch];
    },
    
    /**
     * Set channel options
     * @param {int} ch
     * @param {int} bufferSize
     * @param {array} yAxis   array of axis limits [min, max]
     * @param {boolean} autoscale
     */
    setChOptions: function(ch, bufferSize, yAxis, autoscale)
    {
        this.chOptions[ch] = {
            bufferSize: bufferSize,
            yAxis: yAxis,
            autoscale: autoscale
        };
        
        // Set manual inputs disabled or not
        if(autoscale){
            this.chSettings[ch].getElement('.config_ymin').set('disabled', 'disabled');
            this.chSettings[ch].getElement('.config_ymax').set('disabled', 'disabled');
        } else {
            this.chSettings[ch].getElement('.config_ymin').erase('disabled');
            this.chSettings[ch].getElement('.config_ymax').erase('disabled');
        }
        
        // Set buffer size
        this.setBufferSize(ch, bufferSize);
    },
    
    /**
     * Sets the default options
     * @param {int} ch
     */
    initChOptions: function(ch)
    {
        this.chOptions[ch] = {
            bufferSize: 200,
            yAxis: [-1.5, 1.5],
            autoscale: true
        };
        this.setBufferSize(ch, 200);
    },
    
    /**
     * Set the buffer size
     * @param {int} ch
     * @param {int} size
     */
    setBufferSize: function(ch,size)
    {
        this.dataBuffer[ch] = [];
        this.dataBuffer[ch].length = size;
        for(var i=0;i<size;i++)
            this.dataBuffer[ch][i] = 0;
        
    },
    
    /**
     * Handles incomming websocket data array
     * @param {websocket.event} evt
     */
    handleData: function(evt)
    {
        // Parse the JSON to an object
        var evtData = JSON.parse(evt.data);
        // Append the data and slice the appropriate part of the concatted arr. 
        for(i = 0; i < this.channels; i++){
            
            // Continue of this state is turned off
            if(!this.states[i])
                continue;
            
            this.dataBuffer[i] = this.dataBuffer[i].concat(evtData[i]);
            this.dataBuffer[i] = this.dataBuffer[i].slice(evtData[i].length, this.dataBuffer[i].length);
        }
    },
    
    /**
     * Scales the y-axis to appropriate limits depending on input data
     */
    doAutoScale: function()
    {
        for(i=0; i<this.channels; i++){
            if(this.chOptions[i].autoscale){
                var yMin = this.dataBuffer[i].min();
                var yMax = this.dataBuffer[i].max();
                var diff = yMax > yMin ? yMax-yMin : 1;
                this.chOptions[i].yAxis = [yMin-0.1*diff,yMax+0.1*diff];
                
                this.chSettings[i].getElement('.config_ymin').set('value', Math.round(100*(yMin-0.1*diff))/100);
                this.chSettings[i].getElement('.config_ymax').set('value', Math.round(100*(yMax+0.1*diff))/100);
            
            }
        }
    },
    
    /**
     * Redraw function, called by timer
     */
    redraw: function()
    {
        
        for(i=0; i<this.channels; i++){

            // Index
            var i_ = this.overlayChannels ? 0 : i;

            // Get the canvas context and clear it
            ctx = this.chContexts[ i_ ];

            // If not overlaying or this is the first path..
            if( !this.overlayChannels || i == 0){
                ctx.clearRect ( 0,0,ctx.canvas.width,ctx.canvas.height );

                // draw axis
                ctx.lineWidth = 1;
                ctx.strokeStyle = '#666';
                var cHeight = this.chContexts[i].canvas.height;
                var cWidth =  this.chContexts[i].canvas.width;
                for(var k=0; k<this.yLines; k++){

                    ctx.beginPath();
                    ctx.moveTo(0,k*cHeight/this.yLines);
                    ctx.lineTo(cWidth,k*cHeight/this.yLines);
                    ctx.stroke();

                    ctx.font="10px Georgia";
                    ctx.fillStyle="#fff";
                    var label = this.chOptions[i].yAxis[1] - k/this.yLines*(this.chOptions[i].yAxis[1]- this.chOptions[i].yAxis[0]);
                    label = Math.round(label*100)/100;
                    ctx.fillText(label,10,k*cHeight/this.yLines);

                }
            }

            
            // Begin drawing of scope line
            ctx.beginPath();
            
            ctx.lineWidth   = 2;
            ctx.strokeStyle = this.strokeStyles[i];
            var xScale  = this.chContexts[i_].canvas.width/(this.chOptions[i_].bufferSize);
            var yScale  = this.chContexts[i_].canvas.height/(this.chOptions[i_].yAxis[1]-this.chOptions[i_].yAxis[0]);
            var yMin    = this.chOptions[i_].yAxis[0];
            
            ctx.moveTo(0,cHeight-yScale*(this.dataBuffer[i][0]-yMin));
            
            for(j=1; j<this.chOptions[i_].bufferSize; j++)
                ctx.lineTo(j*xScale, cHeight-yScale*(this.dataBuffer[i][j]-yMin));
            
            ctx.stroke();
        };
    },
    
    /**
     * Handle resize of window
     */
    handleResize: function()
    {
        // update window parameters
        this.scopeWidth = this.container.getSize().x-40;
        var ratio = Math.max(3, this.overlayChannels ? 2 : this.channels*2);
        var ratio = Math.max(3, this.overlayChannels ? 2 : this.channels*2);
        this.scopeHeight = Math.round(this.scopeWidth/ratio);
        
        this.chCanvasses.each(function(el){
            el.width = this.scopeWidth;
            el.height = this.scopeHeight;
        }.bind(this));
    },
    
    /**
     * Update config when 'update' button is clicked
     * @param {int} ch channel number
     */
    updateConfig: function(ch)
    {
        var bufferSize, yAxis, autoscale;

        targets = this.overlayChannels ? Array.apply(null, {length: this.channels}).map(Number.call, Number) : [ ch ];
        ch     = this.overlayChannels ? 0 : ch;

        bufferSize = parseInt(this.chSettings[ch].getElement('.config_samples').get('value'));
        yAxis = [parseFloat(this.chSettings[ch].getElement('.config_ymin').get('value')), parseFloat(this.chSettings[ch].getElement('.config_ymax').get('value'))];
        autoscale = this.chSettings[ch].getElement('.config_autoscale').get('checked');

        targets.each(function(target){
            this.setChOptions(target, bufferSize, yAxis, autoscale);
        }.bind(this));



    },
    
    /**
     * Copy buffer alert
     * @param {int} ch channel number
     */
    copyBuffer: function(ch)
    {
        download(this.dataBuffer[ch].join(','));
    }
    
});

function download(content) {
    
    if(Browser.name == 'ie' && Browser.version < 10){
        alert('Dit werkt alleen op Internet Explorer 10 of hoger, Chrome, Firefox, Opera of Safari');
        return false;
    }
    
       
    var blob = new Blob([content], {
        type: "text/csv;charset=utf-8;",
    });
    saveAs(blob, "scopedata.csv");
    
    
}